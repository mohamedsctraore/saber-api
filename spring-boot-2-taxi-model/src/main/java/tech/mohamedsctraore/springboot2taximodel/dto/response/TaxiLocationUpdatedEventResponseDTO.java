package tech.mohamedsctraore.springboot2taximodel.dto.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

/**
 * @author Mohamed Traore 
*/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TaxiLocationUpdatedEventResponseDTO {

    private String taxiId;

}
