package tech.mohamedsctraore.springboot2taximodel.enums;

/**
 * @author Mohamed Traore 
*/
public enum TaxiType {
    MINI,NANO,VAN;
}
