package tech.mohamedsctraore.springboot2taximodel.dto.response;

import tech.mohamedsctraore.springboot2taximodel.enums.TaxiStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

/**
 * @author Mohamed Traore 
*/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TaxiStatusDTO {
    private String taxiId;

    private TaxiStatus status;
}
