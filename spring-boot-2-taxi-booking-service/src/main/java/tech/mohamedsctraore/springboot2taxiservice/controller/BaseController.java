package tech.mohamedsctraore.springboot2taxiservice.controller;

import tech.mohamedsctraore.springboot2taximodel.dto.response.ErrorDTO;
import tech.mohamedsctraore.springboot2taxiservice.exception.TaxiBookingIdNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * @author Mohamed Traore 
*/
@ControllerAdvice
public class BaseController {

    @ExceptionHandler(TaxiBookingIdNotFoundException.class)
    public ResponseEntity<ErrorDTO> handleTaxiBookingIdNotFoundException(TaxiBookingIdNotFoundException e) {
        return new ResponseEntity<ErrorDTO>(new ErrorDTO(e.getMessage(), HttpStatus.BAD_REQUEST.value()), HttpStatus.BAD_REQUEST);
    }
}
