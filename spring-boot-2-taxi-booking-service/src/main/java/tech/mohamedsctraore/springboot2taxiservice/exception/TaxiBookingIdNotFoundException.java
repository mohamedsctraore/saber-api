package tech.mohamedsctraore.springboot2taxiservice.exception;

/**
 * @author Mohamed Traore 
*/
public class TaxiBookingIdNotFoundException extends RuntimeException {

    public TaxiBookingIdNotFoundException(String message) {
        super(message);
    }

    public TaxiBookingIdNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

}
