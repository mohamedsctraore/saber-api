package tech.mohamedsctraore.springboot2taximodel.dto.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Mohamed Traore 
*/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class LocationDTO {

    private Double latitude;

    private Double longitude;

    private String name;

}
