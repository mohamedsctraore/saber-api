package tech.mohamedsctraore.springboot2taxiservice.exception;

/**
 * @author Mohamed Traore 
*/
public class TaxiIdNotFoundException extends RuntimeException {
    public TaxiIdNotFoundException(String message) {
        super(message);
    }

    public TaxiIdNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}
