package tech.mohamedsctraore.springboot2taxiservice.repo;

import tech.mohamedsctraore.springboot2taxiservice.model.TaxiBooking;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Mohamed Traore 
*/
@Repository
public interface TaxiBookingRepository extends CrudRepository<TaxiBooking, String> {

}
