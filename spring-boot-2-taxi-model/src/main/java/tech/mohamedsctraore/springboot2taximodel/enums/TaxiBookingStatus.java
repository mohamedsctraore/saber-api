package tech.mohamedsctraore.springboot2taximodel.enums;

/**
 * @author Mohamed Traore 
*/
public enum TaxiBookingStatus {
    ACTIVE,CANCELLED,COMPLETED;
}
