package tech.mohamedsctraore.springboot2taximodel.dto.request;

import tech.mohamedsctraore.springboot2taximodel.enums.TaxiType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

/**
 * @author Mohamed Traore 
*/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TaxiRegisterEventDTO {
    private String taxiId = UUID.randomUUID().toString();

    private TaxiType taxiType;
}
