package tech.mohamedsctraore.springboot2taximodel.dto.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Mohamed Traore 
*/
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ErrorDTO {

    private String message;

    private Integer statusCode;

}
